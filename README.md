# LCD

LCD-8bits command via ATmega328 via OlimexINO 32U4 :

```
	-------------             ---------------- 	          --------
       | ATmega328   |	         | OlimexINO 32U4 |              | LCD    |
       |	     |	         |	          |		 |        |
       |             |           |       GND      |------------->|Vss    1|
       |             |           |       VCC      |------------->|Vdd    2|
       |             |           |     GND/VCC    |------ vR --->|V0     3| (potentiomètre du contraste)
       |	     |	         |	          |		 |        |
       |          PF7|---------->|       A0       |------------->|RS     4|
       |             |           |       GND      |------------->|R/W    5|
       |          PF6|---------->|       A1       |------------->|EN     6|
       |	     |	         |	          |		 |        |
       |          PD2|---------->|       D0       |------------->|D0     7| (LSB)
       |          PD3|---------->|       D1       |------------->|D1     8|
       |          PD1|---------->|       D2       |------------->|D2     9|
       |          PD0|---------->|       D3       |------------->|D3    10|      
       |          PD4|---------->|       D4       |------------->|D4    11|
       |          PC6|---------->|       D5       |------------->|D5    12|
       |          PD7|---------->|       D6       |------------->|D6    13|
       |          PE6|---------->|       D7       |------------->|D7    14| (MSB)
       |	     |	         |	          |		 |        |
       |             |           |       VCC      |------------->|LED+  15|
       |             |           |       GND      |------------->|LED-  16|
       |	     |	         |	          |		 |        |
	-------------	          ---------------- 	          --------

```

Compilation : `avr-gcc -mmcu=atmega32u4 -Os -Wall -o coin.o coin.c`

Téléversage : `avrdude -c avr109 -b57600 -D -p atmega32u4 -P /dev/ttyACM0 -e -U flash:w:coin.o`

## Datasheets

* [ATmega](http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7766-8-bit-AVR-ATmega16U4-32U4_Datasheet.pdf)
* [OlimexINO](https://www.olimex.com/Products/Duino/AVR/OLIMEXINO-32U4/resources/OLIMEXINO-32U4.pdf)
* [LCD (44780)](https://fr.wikipedia.org/wiki/HD44780)

