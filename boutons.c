/*
	D8 --> PB4 --> Bouton retour menu (bleu)
        D9 --> PB5 --> Bouton Compteur (noir)
	D10 --> PB6 --> Bouton Reminder (rouge)	
*/


void initBoutons(){
	DDRB &= ~(1<<PORTB4); // D8 (bleu)
	DDRB &= ~(1<<PORTB5); // D9 (noir)
	DDRB &= ~(1<<PORTB6); // D10 (rouge)

	// Pull UP
	PORTB |= (1<<PORTB4);
	PORTB |= (1<<PORTB5);
	PORTB |= (1<<PORTB6);

	// Init des PINS
	PINB &= ~(1<<PINB4);
	PINB &= ~(1<<PINB5);
	PINB &= ~(1<<PINB6);	
}
