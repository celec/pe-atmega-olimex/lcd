#include <avr/io.h>
#define F_CPU 16000000UL
#include <util/delay.h>
#include "lcd.c"

int main(void){
	char a[]={"Good morning!"};

	init();
	boot();

   	data('H');
    	data('e');
    	data('l');
    	data('l');
    	data('o');
    	data(' ');
    	data('w');
    	data('o');
    	data('r');
    	data('l');
    	data('d');

    	command(0xC0);	// Go to Next line and display Good Morning
    	for(short i=0 ; a[i] != 0 ; i++)
    	{
        	data(a[i]);
    	}

    	while(1);

	return 0;
}
