#include <avr/io.h>
#define F_CPU 16000000UL
#include <util/delay.h>
#include "lcd.c"
#include <string.h>

#define columns 19


void scroll(char *c){
	int len = strlen(c);
	char tmp = c[0];
	for (int j=0 ; j<columns ; j++) {
		data(c[j]);
	}		
	for (int i=0;i<len-1; i++) {
		c[i]=c[i+1];
	}
	c[len-1]= tmp;
	command(0x02);
}

int main(){
	init();
	boot();

	while(1) {
		scroll("You have a doctor appointment next Monday. ");
	}
	return 0;
}
