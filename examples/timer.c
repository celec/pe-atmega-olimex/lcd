#include <avr/io.h>
#define F_CPU 16000000UL
#include <util/delay.h>
#include "lcd.c"

#define rows 2
#define columns 16

void disp(char *c){
	// for centering:
	for (int k=0 ; k<(columns/2-1); k++){
		data(' ');
	} // end centering

	for (int i=0 ;i<5;i ++) {
		data(c[i]);
	}
}

void sec(char *time, const char *num){
        for (int i=0 ; i<=9 ; i++) {
        	time[4] = num[i];
        	disp(time);
                _delay_ms(1000);
	}
}

void sec10(char *time, const char *num){
        for (int j=0 ; j<=5 ; j++) {
                time[3] = num[j];
                sec(time, num);
        }
}

void min(char *time, const char *num){
        for (int k=0 ; k<=9 ; k++) {
                time[1] = num[k];
                sec10(time, num);
        }
}

void min10(char *time, const char *num){
        for (int l=0 ; l<=5 ; l++) {
                time[0] = num[l];
                min(time, num);
        }
}

int main(void) {
	init(); boot();
	const char* foo = "0123456789";
	char t[]={'0','0',':','0','0'};
	while (1){
		min10(t,foo);
	}
	return 0;
}
