 /*

	-------------             ---------------- 	          --------
       | ATmega328   |	         | OlimexINO 32U4 |              | LCD    |
       |	     |	         |	          |		 |        |
       |             |           |       GND      |------------->|Vss    1|
       |             |           |       VCC      |------------->|Vdd    2|
       |             |           |     GND/VCC    |------ vR --->|V0     3| (potentiomètre du contraste)
       |	     |	         |	          |		 |        |
       |          PF7|---------->|       A0       |------------->|RS     4|
       |             |           |       GND      |------------->|R/W    5|
       |          PF6|---------->|       A1       |------------->|EN     6|
       |	     |	         |	          |		 |        |
       |          PD2|---------->|       D0       |------------->|D0     7| (LSB)
       |          PD3|---------->|       D1       |------------->|D1     8|
       |          PD1|---------->|       D2       |------------->|D2     9|
       |          PD0|---------->|       D3       |------------->|D3    10|      
       |          PD4|---------->|       D4       |------------->|D4    11|
       |          PC6|---------->|       D5       |------------->|D5    12|
       |          PD7|---------->|       D6       |------------->|D6    13|
       |          PE6|---------->|       D7       |------------->|D7    14| (MSB)
       |	     |	         |	          |		 |        |
       |             |           |       VCC      |------------->|LED+  15|
       |             |           |       GND      |------------->|LED-  16|
       |	     |	         |	          |		 |        |
	-------------	          ---------------- 	          --------


*/
#include "lcd.h"

void draw(char x){
	// D0-D1
	PORTD |= (x & 0x3) << 2;
	PORTD &= (((x & 0x3) << 2) | 0xF3);

	// D2
	PORTD |= (x & 0x4) >> 1;
	PORTD &= (((x & 0x4) >> 1) | 0xFD);

	// D3
	PORTD |= (x & 0x8) >> 3;
	PORTD &= (((x & 0x8) >> 3) | 0xFE);

	// D4
	PORTD |= (x & 0x10);
	PORTD &= ((x & 0x10) | 0xEF);

	// D5
	PORTC |= (x & 0x20) << 1;
	PORTC &= (((x & 0x20) << 1) | 0xBF);

	// D6
	PORTD |= (x & 0x40) << 1;
	PORTD &= (((x & 0x40) << 1) | 0x7F);

	// D7
	PORTE |= (x & 0x80) >> 1;
	PORTE &= (((x & 0x80) >> 1) | 0xBF);
}

void init(void) {
	// innit registre
	DDRD |= 1 << PORTD2; // D0
	DDRD |= 1 << PORTD3; // D1
	DDRD |= 1 << PORTD1; // D2
	DDRD |= 1 << PORTD0; // D3
	DDRD |= 1 << PORTD4; // D4
	DDRC |= 1 << PORTC6; // D5
	DDRD |= 1 << PORTD7; // D6
	DDRE |= 1 << PORTE6; // D7
	DDRF |= 1 << PORTF7; // A0 --> RS
	DDRF |= 1 << PORTF6; // A1 --> E
	//DDRF |= 1 << PORTF5; // A2 --> R/W

	// mise à 0
	PORTD &= ~(1 << PORTD2); // D0
	PORTD &= ~(1 << PORTD3); // D1
	PORTD &= ~(1 << PORTD1); // D2
	PORTD &= ~(1 << PORTD0); // D3
	PORTD &= ~(1 << PORTD4); // D4
	PORTC &= ~(1 << PORTC6); // D5
	PORTD &= ~(1 << PORTD7); // D6
	PORTE &= ~(1 << PORTE6); // D7
	PORTF |= (1 << PORTF7); // RS
	PORTF &= ~(1 << PORTF6); // E
	//PORTF &= ~(1 << PORTF5); // R/W	


}

void command(char cmd){
	draw(cmd);
	PORTF &= ~(1 << PORTF7); // LOW --> RS
	//PORTF &= ~(1 << PORTF5); // LOW --> R/W
	PORTF |= (1 << PORTF6); // HIGH --> E
	_delay_ms(10);
	PORTF &= ~(1 << PORTF6); // LOW --> E
	_delay_ms(10);

}

void data(char dat){
	draw(dat);
	PORTF |= (1 << PORTF7); // HIGH --> RS
	//PORTF &= ~(1 << PORTF5); // LOW --> R/W
	PORTF |= (1 << PORTF6); // HIGH --> E
	_delay_ms(10);
	PORTF &= ~(1 << PORTF6); // LOW --> E
	_delay_ms(10);
}

void boot(void){
        _delay_ms(20);
        ///////////// Reset process from data sheet /////////
        command(0x30);
        _delay_ms(5);
        command(0x30);
        _delay_ms(1);
        command(0x30);
        _delay_ms(10);
        /////////////////////////////////////////////////////
        command(0x38);  // 2 Line, 5*7 display, 8 bit
        command(0x0C);  // Display ON / Cursor ON / Cursor clignotant
        command(0x01);  // Clear
        command(0x06);  // Entry set mode
}

