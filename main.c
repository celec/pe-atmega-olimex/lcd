#include <avr/io.h> // GPIO struc AVR
#define F_CPU 16000000UL // freq du quartz à 16MHz
#include <util/delay.h> // _delay_ms/us();
#include "lcd.c" // init(); boot(); data(); command();
#include "boutons.c" // initBoutons();
#include <string.h> // string stuff
#include "main.h" // protos & columns

int main() {
	init();
	boot();
	initBoutons();
	const char* ints = "0123456789"; // dico des chiffres
	char t[]={'0','0',':','0','0'}; // string du timer
	char menu[] = {"Bouton bleu : menu | Bouton noir : rappel | bouton rouge : chrono "}; // menu
	char mess[] = {"Tu as surement oublie mais tu as un rendez-vous chez le dentiste aujourd'hui. "}; // message du scroll

	while (1) {
		
		// Menu
		while (((PINB & (1<<PINB5)) == 0x20) & ((PINB & (1<<PINB6)) == 0x40) & ((PINB & (1<<PINB4)) == 0x10)){
			scroll(menu);	
		}
		
		// Retour menu
		if ((PINB & (1<<PINB4)) != 0x10) {
			load();
			while (((PINB & (1<<PINB5)) == 0x20) & ((PINB & (1<<PINB6)) == 0x40)){
				scroll(menu);
			}
		}
		
		// Scroll
		if ((PINB & (1<<PINB5)) != 0x20) {
			load();						
			while (((PINB & (1<<PINB4)) == 0x10) & ((PINB & (1<<PINB6)) == 0x40)){
				scroll(mess);
			}
               }
               
              
	       	// Timer	
		if ((PINB & (1<<PINB6)) != 0x40) {
			load();	
			while (((PINB & (1<<PINB4)) == 0x10) & ((PINB & (1<<PINB5)) == 0x20)){
				min10(t,ints);
			}
		}	
					
	}

	return 0;
}

void scroll(char *c){
	int len = strlen(c);
	char tmp = c[0];
	for (int j=0 ; j<columns-1 ; j++) {
		data(c[j]);
	}		
	for (int i=0;i<len-1; i++) {
		c[i]=c[i+1];
	}
	c[len-1]= tmp;
	command(0x02);
}

void disp(char *c){
	
	command(0x02); // début de la 1e ligne
	
	// for centering:
	for (int k=0 ; k<(columns/2-3); k++){
		data(' ');
	} // end centering

	for (int i=0 ;i<5;i ++) {
		data(c[i]);
	}
}

void dispDeux(char *c){

        command(0xC0); // début de la 2e ligne

        // for centering:
        for (int k=0 ; k<(columns/2-3); k++){
                data(' ');
        } // end centering

        for (int i=0 ;i<5;i ++) {
                data(c[i]);
        }
}

void sec(char *time, const char *num){
        for (int i=0 ; i<=9 ; i++) {
        	time[4] = num[i];
        	disp(time);
		_delay_ms(400);
           	if ((((PINB & (1<<PINB4)) != 0x10) | ((PINB & (1<<PINB5)) != 0x20))){break;}
		if ((PINB & (1<<PINB6)) != 0x40) {
		 	dispDeux(time);
		}
		_delay_ms(400);
	}
}

void sec10(char *time, const char *num){
        for (int j=0 ; j<=5 ; j++) {
                time[3] = num[j];
                sec(time, num);
             	if ((((PINB & (1<<PINB4)) != 0x10) | ((PINB & (1<<PINB5)) != 0x20))){break;}   
        }
}

void min(char *time, const char *num){
        for (int k=0 ; k<=9 ; k++) {
                time[1] = num[k];
                sec10(time, num);
                if ((((PINB & (1<<PINB4)) != 0x10) | ((PINB & (1<<PINB5)) != 0x20))){break;}  
        }
}

void min10(char *time, const char *num){
        for (int l=0 ; l<=5 ; l++) {
                time[0] = num[l];
                min(time, num);
                if ((((PINB & (1<<PINB4)) != 0x10) | ((PINB & (1<<PINB5)) != 0x20))){break;}  
        }
}

void load(){
	command(0x01);
	data('[');
	_delay_ms(50);
	data(' ');
	_delay_ms(60);
	data('/');
	_delay_ms(70);
	data('/');
	_delay_ms(150);
	data('/');
	_delay_ms(70);
	data('/');
	_delay_ms(150);
	data('/');
	_delay_ms(70);
	data('/');
	_delay_ms(150);
	data('/');
	_delay_ms(70);
	data('/');
	_delay_ms(150);
	data('/');
	_delay_ms(200);
	data('/');
	_delay_ms(150);
	data('/');
	_delay_ms(70);
	data('/');
	_delay_ms(70);
	data('/');
	_delay_ms(150);
	data('/');
	_delay_ms(70);
	data('/');
	_delay_ms(300);
	data('/');
	_delay_ms(300);
	data(' ');
	_delay_ms(300);
	data(']');
	_delay_ms(300);
	command(0x01);
}


